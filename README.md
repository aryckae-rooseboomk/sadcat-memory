# SadCat Rescue
A project by Aryckae and Rooseboomk
made with react as a fun pair programming side project.

# functionalities
You can choose how many cards you wanna search.
You can choose between 2 en 100 different cards.
You can search 2 of the same pictures.
You can see the pictures you found on the right side of your screen.
You can navigate between the found pictures by using the arrow buttons.
You can restart the game after you found all the cards.
You can see when you found all the same pictures.
You can still see the found pictures until you start a new game.

You cannot hide a picture after you turned him around unless you picked a second picture or you picked a picture beforehand.
you cannot click on the same picture twice to make fool the game that you found the same picture.
