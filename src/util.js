export function getImage(id) {
  const folder = process.env.PUBLIC_URL + "/assets/media/sadcats/";
  return `${folder}${id}.png`;
}
