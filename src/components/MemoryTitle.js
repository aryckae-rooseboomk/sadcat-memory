import * as React from "react";

class MemoryTitle extends React.Component {

    render() {
      return (
        <header>
            <h1>{this.props.title}</h1>
        </header>
      );
    }
}


export default MemoryTitle;