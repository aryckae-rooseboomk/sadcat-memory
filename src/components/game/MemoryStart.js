import * as React from "react";

class MemoryStart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        value: (this.props.default) ? this.props.default : 5,
        min: (this.props.min) ? this.props.min : 2,
        max: (this.props.max) ? this.props.max : 100
    }
    
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
      this.setState({value: e.target.valueAsNumber});
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.setCount(this.state.value);

  }

  render() {
    return (
      <form>
        <label>
          Amount of SadCat pairs:
          <input type="range" value={this.state.value} min={this.state.min} max={this.state.max} onChange={this.handleChange}/>
          <input type="number" value={this.state.value} min={this.state.min} max={this.state.max} onChange={this.handleChange}/>
        </label>
        <input type="submit" onClick={this.handleSubmit}></input>
      </form>
    );
  }
}

export default MemoryStart;
