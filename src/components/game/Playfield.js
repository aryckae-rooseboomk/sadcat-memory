import * as React from "react";
import Card from "./Card";

class Playfield extends React.Component {
    constructor(props) {
        super(props);
        this.toggleCard = this.toggleCard.bind(this);
    }

    toggleCard(id) {
        if (this.props.toggledCards.length === 0 || this.props.toggledCards[0].id !== id) {
            let newCards = this.props.cards.slice();
            let card = newCards.find((card) => card.id === id);
            card.visible = !card.visible;

            let newToggledCards = this.props.toggledCards.slice();
            if (this.props.toggledCards.length >= 2) {
                newToggledCards = [card];
                newCards = toggleCardsVisibility(newCards, this.props.toggledCards);
            } else {
                newToggledCards.push(card);
            }
            this.props.cardsUpdate(newCards, newToggledCards)
        }
    }

    render() {
        let cards;
        if (this.props.cards && this.props.cards.length > 0) {
            cards = this.props.cards.map((card) => {
                return <Card key={card.id} data={card} toggleVisible={this.toggleCard} />;
            }
            );
        }

        return <div className="playField">{cards}</div>;
    }
}

function toggleCardsVisibility(cards, toToggle) {
    cards.forEach((c) => {
        toToggle.forEach((t) => {
            if (c.id === t.id) {
                c.visible = !c.visible;
            }
        });
    });

    return cards;
}

export default Playfield;
