import * as React from "react";
import { getImage } from "../../util";
import arrowUp from "../../icons/arrowUp.svg"; 
import arrowDown from "../../icons/arrowDown.svg";

class FoundCarousel extends React.Component {
  constructor(props) {
    super(props);
    this.state = { listIndex: 0, listSize: (this.props.listSize) ? this.props.listSize : 3 };
    this.scroll = this.scroll.bind(this);
  }

  scroll(up) {
    let index = this.state.listIndex;
    index += (up) ? -this.state.listSize : this.state.listSize;
    if (index < 0) index = 0;
    else if (index > this.props.images.length - this.state.listSize) index = this.props.images.length - this.state.listSize;
    this.setState({ listIndex: index });
  }

  render() {
    const sadcat = "sadcat ";
    let cards = this.props.images.filter((image, idx) => this.state.listIndex <= idx && idx < this.state.listIndex + this.state.listSize)
      .map((image) => (
        <li key={image}>
          <img
            src={getImage(image)}
            alt={sadcat + image}
            title={sadcat + image}
          />
        </li>
      ));
    return (
      <section>
        <h2>{this.props.title}</h2>
        <button onClick={() => this.scroll(true)}><img src={arrowUp} alt="Arrow Up"></img></button>
        <ul>{cards}</ul>
        <button onClick={() => this.scroll(false)}><img src={arrowDown} alt="Arrow Down"></img></button>
      </section>
    );
  }
}

export default FoundCarousel;
