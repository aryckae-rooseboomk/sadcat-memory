import * as React from "react";
import {getImage} from "../../util.js";

class Card extends React.Component {
  render() {
    const description = `sadcat ${this.props.data.img}`;
    return (
      <figure onClick={() => this.props.toggleVisible(this.props.data.id)}>
        <img
            src={getImage(this.props.data.img)}
            alt={description}
            title={description}
            className={(this.props.data.visible) ? "" : "hidden"}
          />
      </figure>
    );
  }
}

export default Card;
