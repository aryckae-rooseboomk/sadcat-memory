import * as React from "react";
import Playfield from "./game/Playfield";
import FoundCarousel from "./game/FoundCarousel";
import MemoryStart from "./game/MemoryStart";

class MemoryGame extends React.Component {
  constructor(props) {
    super(props);

    this.updateCards = this.updateCards.bind(this);
    this.start = this.start.bind(this);
    this.reset = this.reset.bind(this);

    this.state = {
      inProgress: false,
      cards: [],
      toggledCards: [],
      foundCards: [],
    };
  }

  start(count) {
    this.setState({
      cards: initCards(count),
      inProgress: true,
      foundCards: []
    });
  }

  updateCards(cards, toggledCards) {
    let newFoundCards = this.state.foundCards.slice();
    if (
      toggledCards.length === 2 &&
      toggledCards[0].img === toggledCards[1].img
    ) {
      toggledCards.forEach((t) => {
        let index = cards.indexOf(t);
        if (index > -1) cards.splice(index, 1);
      });
      newFoundCards.push(toggledCards[0].img);
      toggledCards = [];
    }

    this.setState({
      cards: cards,
      toggledCards: toggledCards,
      foundCards: newFoundCards,
    });
  }

  reset() {
    const state = {
      inProgress: false,
      cards: [],
      toggledCards: [],
    };

    this.setState(state);
  }

  render() {
    return (
      <main>
        {this.state.cards.length === 0 && !this.state.inProgress && (
          <MemoryStart setCount={this.start} />
        )}
        {this.state.cards.length > 0 && (
          <Playfield
            cards={this.state.cards}
            toggledCards={this.state.toggledCards}
            cardsUpdate={this.updateCards}
          />
        )}
        {this.state.cards.length === 0 && this.state.inProgress && (
          <section>
            <h2>Congratulations! You rescued all the sadcats!</h2>
            <button onClick={this.reset}>Restart</button>
          </section>
        )}
        <FoundCarousel title="SadCats Rescued" images={this.state.foundCards} />
      </main>
    );
  }
}

function initCards(count) {
  let cards = [];
  let img = 0;
  for (let i = 0; i < count * 2; i++) {
    img = i % 2 === 0 ? newImage(cards) : img;
    let card = { id: i, img: img, visible: false };
    cards.push(card);
  }
  return shuffleArray(cards);
}

function newImage(data) {
  let images = data && data.length > 0 ? data.map((c) => c.img) : [];
  let rand;
  do {
    rand = Math.floor(Math.random() * 100);
  } while (images.indexOf(rand) !== -1);

  return rand;
}

function shuffleArray(cards) {
  // Fisher-Yates Shuffle
  let res = cards.slice();
  for (let i = res.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * i);
    const a = res[i];
    const b = res[j];
    res[j] = a;
    res[i] = b;
  }
  return res;
}

export default MemoryGame;
