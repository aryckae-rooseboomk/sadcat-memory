import MemoryGame from "./components/MemoryGame";
import MemoryTitle from "./components/MemoryTitle";

function App() {
  

  return (
    <div className="App">
      <MemoryTitle title = "SadCat Rescue"/>
      <MemoryGame/>
    </div>
  );
}

export default App;
